import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';

import TodoApp from 'TodoApp';

describe('TodoApp', () => {
  it('should exist', () => {
    expect(TodoApp).toExist();
  });

  it('should add todo to the todos state on handleAddTodo', () => {
    const textTodo = 'Learn React!';
    const todoApp = TestUtils.renderIntoDocument(<TodoApp />);

    todoApp.setState({todos: []});
    todoApp.handleAddTodo(textTodo);

    expect(todoApp.state.todos[0].text).toBe(textTodo);
    expect(todoApp.state.todos[0].createdAt).toBeA('number');
  });

  it('should toggle completed value and add completedAt timestamp when handleToggle called', () => {
    const todoData = {
      id: 11,
      text: 'Do something',
      completed: false,
      createdAt: 0,
      completedAt: undefined
    };
    const todoApp = TestUtils.renderIntoDocument(<TodoApp />);

    todoApp.setState({ todos: [todoData] });
    expect(todoApp.state.todos[0].completed).toBe(false);
    todoApp.handleToggle(11);
    expect(todoApp.state.todos[0].completed).toBe(true);
    expect(todoApp.state.todos[0].completedAt).toBeA('number');
  });

  it('should toggle completed value and delete completedAt timestamp when handleToggle called', () => {
    const todoData = {
      id: 11,
      text: 'Do something',
      completed: true,
      createdAt: 0,
      completedAt: 11111
    };
    const todoApp = TestUtils.renderIntoDocument(<TodoApp />);

    todoApp.setState({ todos: [todoData] });
    expect(todoApp.state.todos[0].completed).toBe(true);
    todoApp.handleToggle(11);
    expect(todoApp.state.todos[0].completed).toBe(false);
    expect(todoApp.state.todos[0].completedAt).toNotExist();
  });
});
