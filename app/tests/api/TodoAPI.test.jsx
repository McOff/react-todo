import expect from 'expect';

import TodoAPI from 'TodoAPI';

beforeEach(() => {
  localStorage.removeItem('todos');
});

describe('TodoAPI', () => {
  it('should exist', () => {
    expect(TodoAPI).toExist();
  });

  describe('setTodos', () => {
    it('should set valid todos array', () => {
      const todos = [{
        id: 123,
        text: 'text',
        completed: false
      }];

      TodoAPI.setTodos(todos);

      const acualTodos = JSON.parse(localStorage.getItem('todos'));
      expect(acualTodos).toEqual(todos);
    });

    it('should nod set invalid todos', () => {
      const todos = { a: 'b' };
      TodoAPI.setTodos(todos);
      const actualTodos = localStorage.getItem('todos');
      expect(actualTodos).toBe(null);
    });
  }); //setTodos

  describe('getTodos', () => {
    it('should return [] on bad storage data', () => {
      const todos = TodoAPI.getTodos();
      expect(todos).toEqual([]);
    });

    it('should return todos from storage', () => {
      const todos = [{
        id: 123,
        text: 'text',
        completed: false
      }];

      localStorage.setItem('todos', JSON.stringify(todos));
      const acualTodos = TodoAPI.getTodos();
      expect(acualTodos).toEqual(todos);
    });
  }); // getTodos

  describe('filterTodos', () => {
    const todos = [{
      id: 1,
      text: 'Some text',
      completed: true
    },{
      id: 2,
      text: 'Another Text',
      completed: false
    },{
      id: 3,
      text: 'New',
      completed: true
    }];
    it('should return all items if showComleted true', () => {
      const filteredTodos = TodoAPI.filterTodos(todos, true, '');
      expect(filteredTodos.length).toBe(3);
    });

    it('should return only non-completed item if shoCompleted not true', () => {
      const filteredTodos = TodoAPI.filterTodos(todos, false, '');
      expect(filteredTodos.length).toBe(1);
    });

    it('should filter todos', () => {
      const filteredTodos = TodoAPI.filterTodos(todos, true, '');
      expect(filteredTodos[0].id).toBe(2);
    });

    it('should filter todos by searchText', () => {
      const filteredTodos = TodoAPI.filterTodos(todos, true, 'text');
      expect(filteredTodos.length).toBe(2);
    });

    it('should return all todos if searchText is empty', () => {
      const filteredTodos = TodoAPI.filterTodos(todos, true, '');
      expect(filteredTodos.length).toBe(3);
    });
  });

});
