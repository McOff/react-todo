import React from 'react';

class AddTodo extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const todo = this.refs.text.value;
    if (todo) {
      this.refs.text.value = '';
      this.props.onAddTodo(todo);
    } else {
      this.refs.text.focus();
    }
  }

  render() {
    return (
        <div className="container__footer">
          <form ref="form" onSubmit={ this.handleSubmit } >
            <input type="text" ref="text" placeholder="Enter todo" />
            <button className="button expanded">Add Todo</button>
          </form>
        </div>
    );
  }
}

export default AddTodo;
