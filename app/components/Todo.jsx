import React from 'react';
import moment from 'moment';

class Todo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {text, id, completed, onToggle, createdAt, completedAt} = this.props;
    const todoClassName = completed ? 'todo todo-completed' : 'todo';
    const createDate = (message, timestamp) => {
      return message + moment.unix(timestamp).format('MMM Do YYYY @ h:mm a');
    }
    return (
      <div className={todoClassName} onClick={() => {
        onToggle(id)
      }}>
        <div>
          <input type="checkbox" checked={completed} />
        </div>
        <div>
          <p>{text}</p>
          <p className="todo__subtext">{createDate('Created', createdAt)}</p>
          <p className="todo__subtext">{ completed && createDate('Completed', completedAt)}</p>
        </div>
      </div>
    );
  }
}

export default Todo;
