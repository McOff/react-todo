import React from 'react';

class TodoSearch extends React.Component {
  constructor(props) {
    super(props);

    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch() {
    const showCompleted = this.refs.showCompleted.checked;
    const searchText = this.refs.searchText.value;

    this.props.onSearch(showCompleted, searchText);
  }

  render() {
    return (
      <div className="container_header">
        <div>
          <input type='text' ref='searchText' placeholder='Search todos' onChange={ this.handleSearch } />
        </div>
        <div>
          <label>
            <input type="checkbox" ref="showCompleted" onChange={this.handleSearch}/>
            Show complited todos
          </label>
        </div>
      </div>
    );
  }
}

export default TodoSearch;
