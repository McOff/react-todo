function add (a, b) {
  return a + b;
}

console.log(add(1, 3));
const elems = [9, 4];

const groupA = ['Oleg', 'Masha'];
const groupB = ['Masha'];
const final = [...groupB, '+', ...groupA];
console.log(add(...elems));
console.log(final);

function sayHi(name, age) {
  console.log(`Hi ${ name }, you are ${ age }`);
}

const person1 = ['Oleg', 31];
const person2 = ['Masha', 26];

sayHi(...person1);
sayHi(...person2);

const finalNames = ['Andrew', ...groupA];

finalNames.forEach((name) => {
  console.log(`Hi ${name}!`);
});
